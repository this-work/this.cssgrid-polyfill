/**
 * CSS Grid Polyfill
 *
 * Includes:
 * - CSS Grid Autoplacement Polyfill from IE 10 to Edge 15 ( IE10 needs a mutation observer polyfill )
 * - Row Gap Polyfill with automatic calculated rows from IE 10 to Edge 15
 *
 * @link      https://www.this.work/
 * @author    Tobias Wöstmann
 * @version   1.0.00
 */

export const cssGridPolyfill = (() => {

    let occupiedCells = {};

    const gridDisplayProp = '-ms-grid';

    return {

        /**
         * Manual init function to call polyfill from outside.
         */
        init: () => {

            if (typeof document.createElement('div').style.grid !== 'string') {

                const gridSelectors = getAllGridSelectors();

                if (gridSelectors.length > 0) {

                    runGridPolyfills(getGridNodes(gridSelectors));

                    setDomChangeListener(gridSelectors);

                    setTimeout(() => {
                        setResizeListener(gridSelectors);
                    }, 0);

                }
            }

        },

        /**
         * Manual update function to call polyfill from outside.
         *
         * @param {Array|String} selector - Array ort String of a Selector.
         */
        update: (selector = gridSelectors) => {

            if (typeof document.createElement('div').style.grid !== 'string') {

                const selectorArray = [].concat(selector);

                runGridPolyfills(getGridNodes(selectorArray));

            }

        }

    };

    /**
     * Get all Selectors with a display: -ms-grid Style from all Stylesheets.
     * Loop all root level rules and go deeper if rule a media query.
     *
     * @return {Array} Array of Selector Strings
     */
    function getAllGridSelectors() {

        const gridSelectors = [];

        let sheetNumber;
        for (sheetNumber = 0; sheetNumber < document.styleSheets.length; sheetNumber++) {

            let ruleNumber;
            const styleSheet = document.styleSheets[sheetNumber];
            const styleSheetRuleCount = styleSheet.cssRules.length;

            for (ruleNumber = 0; ruleNumber < styleSheetRuleCount; ruleNumber++) {

                const rule = styleSheet.cssRules[ruleNumber];

                if (rule.style !== undefined) {

                    if (rule.style.display === gridDisplayProp) {
                        gridSelectors.push(rule.selectorText);
                    }

                } else {

                    let childRuleNumber;
                    const childRuleCount = rule.cssRules.length;

                    for (childRuleNumber = 0; childRuleNumber < childRuleCount; childRuleNumber++) {

                        const styleRule = rule.cssRules[childRuleNumber];

                        if (styleRule.style !== undefined) {

                            if (styleRule.style.display === gridDisplayProp) {

                                gridSelectors.push(styleRule.selectorText);

                            }
                        }

                    }
                }

            }

        }

        return gridSelectors;

    }

    /**
     * Get all Nodes from the gridSelectors Array.
     *
     * @param {Array} gridSelectors - Array of Selector Strings
     * @return {NodeList} List of grid nodes
     */
    function getGridNodes(gridSelectors) {

        return document.querySelectorAll(gridSelectors.join(', '));

    }

    /**
     * Run/Update/Execute all CSS Grid Polyfills for every Grid on the Page.
     *
     * @param {Array} gridNodes - Array of Grid Nodes
     */
    function runGridPolyfills(gridNodes) {

        for (const grid of gridNodes) {

            const cells = getGridCells(grid);

            autoplacementPolyfill(grid, cells);

            rowGapPolyfill(grid);

        }

    }

    /**
     * Register a Resize Listener via matchMedia to increase the performance.
     * Should be executed in a new Thread to prevent a long waittime.
     *
     * @param {Array} gridSelectors - Array of Selector Strings
     */
    function setResizeListener(gridSelectors) {

        const grids = getGridNodes(gridSelectors);
        const breakpoints = getGridBreakpoints(grids);

        let breakpointKey;
        for (breakpointKey = 0; breakpointKey < Object.keys(breakpoints).length; breakpointKey++) {

            const breakpoint = Object.keys(breakpoints)[breakpointKey];
            const matchMediaList = window.matchMedia(breakpoint);

            matchMediaList.addListener(() => {

                runGridPolyfills(getGridNodes(gridSelectors));

            });

        }

    }

    /**
     * Register a Resize Listener via MutationObserver only for < IE 10.
     *
     * @param {Array} gridSelectors - Array of Selector Strings
     */
    function setDomChangeListener(gridSelectors) {

        if ('MutationObserver' in window) {

            const polyfillObserverConfig = {
                attributes: false,
                childList: true,
                subtree: true,
                characterData: false
            };

            const polyfillObserver = new MutationObserver(mutationRecords => {

                let mutationRecordNumber;
                let mutationRecordLength;
                for (mutationRecordNumber = 0, mutationRecordLength = mutationRecords.length;
                     mutationRecordNumber < mutationRecordLength;
                     mutationRecordNumber++) {

                    const mutationRecord = mutationRecords[mutationRecordNumber];

                    if (getComputedStyle(mutationRecord.target).display === gridDisplayProp) {

                        runGridPolyfills([mutationRecord.target]);

                    }

                    if (mutationRecord.addedNodes && mutationRecord.addedNodes.length > 0) {
                        for (const addedNode of mutationRecord.addedNodes) {
                            if (addedNode.nodeType === 1 && addedNode.querySelectorAll(gridSelectors).length > 0) {
                                runGridPolyfills(addedNode.querySelectorAll(gridSelectors));
                            }
                        }
                    }

                }

            });

            polyfillObserver.observe(document.body, polyfillObserverConfig);

        }

    }

    /**
     * Return all mediaquery breakpoints that contain column/row css
     * stylings. Loop all Queryrules in all Stylesheets and match it with
     * all cells of all grids. Can be a slow function.
     *
     * @param {NodeList} grids - List of grid nodes
     * @return {Object} All Mediaquerys with grid child declarations.
     */
    function getGridBreakpoints(grids) {

        const breakpoints = {};
        const gridRules = [];
        const cssRulesInQuerys = [];

        let sheetNumber;
        for (sheetNumber = 0; sheetNumber < document.styleSheets.length; sheetNumber++) {

            const styleSheet = document.styleSheets[sheetNumber];

            let mediaRuleNumber;
            let mediaRuleLength;
            for (mediaRuleNumber = 0, mediaRuleLength = styleSheet.cssRules.length; mediaRuleNumber < mediaRuleLength; mediaRuleNumber++) {

                const rule = styleSheet.cssRules[mediaRuleNumber];

                if (rule.media !== undefined) {

                    let cssRulesNumber;
                    let cssRulesLength;
                    for (cssRulesNumber = 0, cssRulesLength = rule.cssRules.length; cssRulesNumber < cssRulesLength; cssRulesNumber++) {

                        cssRulesInQuerys.push(rule.cssRules[cssRulesNumber]);

                    }

                }

            }

        }

        for (const grid of grids) {

            let ruleNumber;
            for (ruleNumber = 0; ruleNumber < cssRulesInQuerys.length; ruleNumber++) {

                if (grid.msMatchesSelector(cssRulesInQuerys[ruleNumber].selectorText)) {

                    gridRules.push(cssRulesInQuerys[ruleNumber]);

                }

            }

            const cells = getGridCells(grid);

            let cellNumber;
            for (cellNumber = 0; cellNumber < cells.length; cellNumber++) {

                const cell = cells[cellNumber];

                let ruleNumber;
                for (ruleNumber = 0; ruleNumber < cssRulesInQuerys.length; ruleNumber++) {

                    if (cell.msMatchesSelector(cssRulesInQuerys[ruleNumber].selectorText)) {

                        gridRules.push(cssRulesInQuerys[ruleNumber]);

                    }

                }

            }

        }

        let gridRuleNumber;
        for (gridRuleNumber = 0; gridRuleNumber < gridRules.length; gridRuleNumber++) {

            const gridRule = gridRules[gridRuleNumber];

            breakpoints[gridRule.parentRule.media.mediaText] = gridRule.parentRule.media.mediaText;

        }

        return breakpoints;

    }

    /**
     * Return all children of a grid. All children nodes a automatically
     * cells in the css grid.
     *
     * @param {Node} grid - Node of the Grid
     * @return {NodeList} All children of the grid.
     */
    function getGridCells(grid) {
        return grid.children;
    }

    /**
     * Return an Object with all Grid Informations of Rows, Columns and a boolean if
     * the element is an grid element.
     *
     * @param {Node} grid - Node of the Grid
     * @return {Object} Grid Data
     */
    function getGridData(grid) {

        const gridStyles = getComputedStyle(grid);
        const gridData = {};

        gridData.isCssGrid = (gridStyles.display === '-ms-grid');
        gridData.rows = gridStyles.msGridRows.split(' ').length;
        gridData.rowsRaw = gridStyles.msGridRows;
        gridData.columns = gridStyles.msGridColumns.split(' ').length;
        gridData.columnsRaw = gridStyles.msGridColumns;

        return gridData;

    }

    /**
     * Run the autoplacement polyfill.
     * Only support actually the grid-flow:row method.
     *
     * @param {Node} grid - Node of the Grid
     * @param {NodeList} cells - All grid cells
     */
    function autoplacementPolyfill(grid, cells) {

        removeAutoplacementAttributes(cells);

        const gridData = getGridData(grid);

        if (gridData.isCssGrid) {

            occupiedCells = {};

            setOccupiedGridCells(cells);

            let rowPosition = 2;

            let cellNumber;

            for (cellNumber = 0; cellNumber < cells.length; cellNumber++) {

                let rowBreak = false;
                let columnPosition = 0;
                let tempRowPosition = rowPosition;
                let cellIsPositioned = false;
                const cell = cells[cellNumber];
                const cellData = getCellData(cell);

                if (!cellData.column || !cellData.row) {

                    if (cellData.column) {

                        cellIsPositioned = true;
                        columnPosition = cellData.column - 2;

                        if (isCellPositionOccupied(cellData.column, rowPosition, cellData)) {
                            rowBreak = true;
                        }

                    }

                    if (cellData.row) {
                        tempRowPosition = cellData.row;
                    }

                    do {

                        columnPosition += 2;

                        if (rowBreak || columnPosition - 1 + cellData.width > gridData.columns) {

                            rowBreak = false;

                            columnPosition = cellData.column ? cellData.column : 2;

                            tempRowPosition += 2;

                        }

                    } while (isCellPositionOccupied(columnPosition, tempRowPosition, cellData));

                    if (cellIsPositioned) {
                        cellData.width += cellData.column;
                        columnPosition = columnPosition - cellData.column;
                    }

                    if (!cellData.row) {
                        rowPosition = tempRowPosition;
                    }

                    cellData.column = columnPosition;
                    cellData.row = tempRowPosition;

                    cell.style.msGridColumn = columnPosition;
                    cell.style.msGridRow = tempRowPosition;

                    columnPosition = columnPosition + cellData.width - 1;

                    setOccupiedGridCell(cellData);

                }

            }

        }

    }

    /**
     * Return an Object with all Cell Informations of Width, Height, column and rows.
     *
     * @param {Node} cell - Node of the Cell
     * @return {Object} Cell Data
     */
    function getCellData(cell) {

        const cellStyles = getComputedStyle(cell);
        const cellData = {};

        cellData.width = transformStyleStringToInt(cellStyles.msGridColumnSpan, 1);
        cellData.height = transformStyleStringToInt(cellStyles.msGridRowSpan, 1);

        cellData.column = transformStyleStringToInt(cellStyles.msGridColumn);
        cellData.row = transformStyleStringToInt(cellStyles.msGridRow);

        if (cellData.column === 1) {
            cellData.column = false;
        }
        if (cellData.row === 1) {
            cellData.row = false;
        }

        return cellData;

    }

    /**
     * Remove all grid style attributes on the given cells.
     *
     * @param {NodeList} cells - Nodes of Cells
     */
    function removeAutoplacementAttributes(cells) {

        let cellNumber;
        for (cellNumber = 0; cellNumber < cells.length; cellNumber++) {

            const cell = cells[cellNumber];

            cell.style.removeProperty('-ms-grid-column');
            cell.style.removeProperty('-ms-grid-row');

        }

    }

    /**
     * Occupie all prepositioned elements to the occupiedCells Object.
     *
     * @param {NodeList} cells - Nodes of all Cells
     */
    function setOccupiedGridCells(cells) {

        let cellNumber;
        for (cellNumber = 0; cellNumber < cells.length; cellNumber++) {

            const cell = cells[cellNumber];
            const cellData = getCellData(cell);

            if (cellData.column && cellData.row) {

                setOccupiedGridCell(cellData);

            }

        }

    }

    /**
     * Add occupied cells in the occupiedCells Object for given cellData.
     * Iterate all cells from the column start to the column end of the cell while
     * iterateing from the row start to the row end of the cell.
     *
     * @param {Object} cellData - Data of Cell that should be blocked
     */
    function setOccupiedGridCell(cellData) {

        let iteratorRow;
        let iteratorColumn;

        for (iteratorRow = cellData.row; iteratorRow < (cellData.row + cellData.height); iteratorRow++) {
            for (iteratorColumn = cellData.column; iteratorColumn < (cellData.column + cellData.width); iteratorColumn++) {

                occupiedCells[iteratorRow + '-' + iteratorColumn] = true;

            }
        }

    }

    /**
     * Return a Boolean if the given cell postion are blocked with a other cell.
     * Iterate all cells from the column start to the column end of the cell while
     * iterateing from the row start to the row end of the cell.
     *
     * @param {Number} cellColumnPosition - column start of the cell that should be positioned
     * @param {Number} cellRowPosition - row start of the cell that should be positioned
     * @param {Object} cellData - Data (width/height) of Cell that should be positioned
     * @return {Boolean} is the cell postion blocked?
     */
    function isCellPositionOccupied(cellColumnPosition, cellRowPosition, cellData) {

        let iteratorRow;
        let iteratorColumn;

        for (iteratorRow = cellRowPosition; iteratorRow < (cellRowPosition + cellData.height); iteratorRow++) {
            for (iteratorColumn = cellColumnPosition; iteratorColumn < (cellColumnPosition + cellData.width); iteratorColumn++) {

                if (occupiedCells[iteratorRow + '-' + iteratorColumn]) {

                    return true;

                }

            }
        }

        return false;

    }

    /**
     * Remove the grid-rows style attributes on the given grid.
     *
     * @param {Node} grid - Node of a grid
     */
    function removeRowGapAttributes(grid) {

        grid.style.removeProperty('-ms-grid-rows');

    }

    /**
     * Run the row gap polyfill.
     * Getting the computed grid rows and add between 2 rows a empty gap row.
     *
     * @param {Node} grid - Node of the Grid
     */
    function rowGapPolyfill(grid) {

        removeRowGapAttributes(grid);

        const gridData = getGridData(grid);
        const rowArray = gridData.rowsRaw.split(' ');
        const rowGap = rowArray[2];
        const rowArrayWithGaps = [];

        let rowArrayKey;
        for (rowArrayKey = 1; rowArrayKey < rowArray.length; rowArrayKey += 2) {

            rowArrayWithGaps.push(rowGap, 'auto');

        }

        rowArrayWithGaps[0] = '0px';

        grid.style.msGridRows = rowArrayWithGaps.join(' ');

    }

    /**
     * Return an Int from a Style String.
     *
     * @param {String} style - Style Value to convert
     * @param {Number} fallback - Fallback when String is not convertable.
     * @return {Number} Style Value as Int
     */
    function transformStyleStringToInt(style, fallback = false) {

        const int = parseInt(style.replace('span ', ''));

        if (!isNaN(int)) {

            return int;

        } else {

            return fallback;

        }

    }

})();
