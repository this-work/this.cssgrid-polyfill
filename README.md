# This - CSS Grid Polyfill
CSS Grid Polyfill for IE 10 to Edge 15.


#### Features
- Row flowed Autoplacement ( IE10 needs a mutation observer polyfill )
- Row Gap Polyfill with automatic calculated rows


#### Install

Install via node

```bash
npm install @this/cssgrid-polyfill --save
```


#### Usage

Include the polyfill in oyur JS with:

```bash
import 'cssgrid-polyfill';
```

The polyfill works automatically and out of the box. Its recommend to use the polyfill with the @this/scss-framework. 
The framework builds some required css to polyfill the missing features.


#### Methods

You can update the autoplacement with the update method.
```bash
cssGridPolyfill.update();
```
